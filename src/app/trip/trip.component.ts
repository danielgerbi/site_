import { Component, OnInit } from '@angular/core';
import { Trip } from "./trip";
import { TripService } from '../trip.service';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {
  private trips: Trip[];

  constructor(private tripService: TripService) { }

  ngOnInit() {
    this.tripService.getTrips()
      .subscribe(trips => this.trips = trips);
  } 
}
