import { Trip } from './trip';

export const TRIPS: Trip[] = [
    { title: 'Greece', description: 'Greece  has an amazing sea, and is also cheap', image: '../../assets/trips-images/greece.jpeg'},
    { title: 'Britain', description: 'Britain is the most winderfull place in Europe', image: '../../assets/trips-images/london.jpg'},
    { title: 'New Zealand', description: 'New zealand is a magic place!', image: '../../assets/trips-images/newZeland.jpg'},
    { title: 'Australia', description: 'where you feel the real face of Nature', image: '../../assets/trips-images/australia.jpg'},
]
