import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from '../login/login.component';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
 // constructor(private modalService: NgbModal) {
 // }
  constructor(public activeModal: NgbActiveModal) {}
  ngOnInit() {
  }

}
