import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TriplistComponent } from './triplist/triplist.component';
import { MainViewComponent } from './main-view/main-view.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full'},
 // {   path: '',                                             },
  { path: 'main', component: MainViewComponent},
  { path: 'logged', component: TriplistComponent}
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
