import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Trip } from './trip/trip';
import { TRIPS } from './trip/mock-trips';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor() { }

  getTrips(): Observable<Trip[]> {
    return of(TRIPS);
  }
}
