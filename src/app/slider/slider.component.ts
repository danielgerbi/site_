import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  sliderImageIndex: number = 0;
  constructor() {
    setInterval(() =>  { if(this.sliderImageIndex== 2) {
this.sliderImageIndex = 0;
    }
      else {
        this.sliderImageIndex = this.sliderImageIndex +1;
      }
      },4000);
    }

  ngOnInit() {
  }

  changeSliderImage(direction: string) {
    const offset = direction === "left" ? -1 : 1; 
    const newIndex = this.sliderImageIndex + offset;
    const fixedIndex = newIndex < 0 ? newIndex + 3 : newIndex;

    this.sliderImageIndex = fixedIndex % 3;
  }
}
