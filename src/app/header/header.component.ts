import { Component, OnInit } from '@angular/core';
import { LoginComponent  } from '../login/login.component';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { MenuComponent  } from '../menu/menu.component';
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/dropdown';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(private modalService: NgbModal) {
  }
open2()  {
  const modalRef = this.modalService.open(MenuComponent);
  modalRef.componentInstance.name = 'menu';
  }
open() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.name = 'login';
  }

}


